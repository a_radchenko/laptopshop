﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaptopShop.Models.Dtos
{
    public class ProductPreviewDto
    {
        public ProductPreviewDto(Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Price = product.Price;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public decimal? Price { get; set; }
    }
}
