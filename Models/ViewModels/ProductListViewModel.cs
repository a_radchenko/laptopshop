﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaptopShop.Models.ViewModels
{
    public class ProductListViewModel
    {
        public ProductListViewModel(List<Product> products)
        {
            Products = products.Select(p => new ProductViewModel(p)).ToList();
        }

        public List<ProductViewModel> Products { get; set; }

        public class ProductViewModel
        {
            public ProductViewModel(Product product)
            {
                Id = product.Id;
                Name = product.Name;
                Price = product.Price == null ? "N/A" : product.Price.ToString();
            }
            public int Id { get; set; }

            public string Name { get; set; }

            public string Price { get; set; }
        }
    }
}
