﻿using System;
using System.Collections.Generic;
using System.Text;
using LaptopShop.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LaptopShop.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Product>().HasData(
                new List<Product>
                {
                    new Product { Id = 1, Name = "Acer", Description = "Description Acer", Price = 40000 },
                    new Product { Id = 2, Name = "Asus", Price = 72000 },
                    new Product { Id = 3, Name = "Xiaomi", Price = 30000 },
                    new Product { Id = 4, Name = "MacBook Air", Price = 150000 },
                    new Product { Id = 5, Name = "HP Omen", Description = "Description HP" }
                }
            );
        }
    }
}
