﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LaptopShop.Data;
using LaptopShop.Models;
using LaptopShop.Models.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LaptopShop.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProductsController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public ProductsController(ApplicationDbContext context)
        {
            this.context = context;
        }

        // GET: api/products/2
        [HttpGet("{id}")]
        public IActionResult GetProduct(int id)
        {
            var product = context.Products.SingleOrDefault(p => p.Id == id);
            if(product != null)
            {
                // TODO: Use AutoMapper
                return Ok(new ProductDto(product));
            } else
            {
                return NotFound();
            }
        }

        // GET: api/products
        [HttpGet]
        public IActionResult GetProducts(string name, string minPrice, string maxPrice)
        {
            var products = context.Products.AsQueryable();
            // Assuming OrdinalIgnoreCase strings equality
            if (!string.IsNullOrEmpty(name))
            {
                products = products.Where(p => string.Equals(p.Name, name, StringComparison.OrdinalIgnoreCase));
            }

            if (minPrice != null)
            {
                decimal parsedMinPrice;
                var isDecimal = decimal.TryParse(minPrice, out parsedMinPrice);
                if (isDecimal)
                {
                    products = products.Where(p => p.Price >= parsedMinPrice);
                }
 
            }

            if (maxPrice != null)
            {
                decimal parsedMaxPrice;
                var isDecimal = decimal.TryParse(maxPrice, out parsedMaxPrice);
                if (isDecimal)
                {
                    products = products.Where(p => p.Price <= parsedMaxPrice);
                }
            }

            var productsDtos = products.Select(p => new ProductPreviewDto(p)).ToList();

            return Ok(productsDtos);
        }
    }
}