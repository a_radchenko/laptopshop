﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LaptopShop.Data;
using LaptopShop.Models;
using LaptopShop.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LaptopShop.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private readonly ApplicationDbContext context;

        public ProductsController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var products = context.Products.ToList();
            var productListViewModel = new ProductListViewModel(products);

            return View(productListViewModel);
        }

        [HttpGet]
        public IActionResult New()
        {
            var productFormViewModel = new ProductFormViewModel();
            ViewBag.Title = "Create";
            return View("ProductForm", productFormViewModel);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var product = context.Products.SingleOrDefault(p => p.Id == id);
            if (product == null)
                return RedirectToAction("Index", "Products");

            var productFormViewModel = new ProductFormViewModel(product);
            ViewBag.Title = "Edit";
            return View("ProductForm", productFormViewModel);
        }

        [HttpPost]
        public IActionResult Save(Product model)
        {
            if (model.Id == 0)
            {
                context.Products.Add(model);
            }
            else
            {
                var existingProduct = context.Products.SingleOrDefault(p => p.Id == model.Id);
                if (existingProduct == null)
                    return StatusCode(404);

                existingProduct.Name = model.Name;
                existingProduct.Description = model.Description;
                existingProduct.Price = model.Price;
            }
            context.SaveChanges();
            return RedirectToAction("Index", "Products");
        }
    }
}